﻿namespace TTS.LibRecognize
{
    public class VoiceTextData
    {
        public string Recognised
        {
            get;
            set;
        }

        public double Rating
        {
            get;
            set;
        }
    }
}