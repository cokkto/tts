﻿using System;
using System.IO;
using TTS.LibWave;

namespace TTS.LibRecognize
{
    internal class VoiceText
    {
        public static VoiceTextData RecordText( int seconds )
        {
            string file = Path.GetTempFileName();// +".flac";
            Recorder rec = new Recorder( file );
            System.Threading.Thread.Sleep( seconds * 1000 );
            rec.Stop();
            while ( !rec.AllDone )
            {
            }
            GC.SuppressFinalize( rec );
            GoogleRecognizer recogn = new GoogleRecognizer();

            GoogleResponse respdata = null;
            try
            {
                respdata = recogn.RecognizeFlac( file );
            }
            catch ( Exception )
            {
                respdata = null;
            }
            if ( respdata == null )
                return null;

            File.Delete( file );
            //Json
            return new VoiceTextData()
            {
                Rating = respdata.hypotheses[0].confidence,
                Recognised = respdata.hypotheses[0].utterance
            };
        }
    }
}