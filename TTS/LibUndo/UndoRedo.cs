﻿using System.Collections.Generic;

namespace TTS.LibUndo
{
    public class UndoRedo
    {
        // undoredo size
        private int size;

        // pointer
        private int idx;

        // top redo
        private int idr;

        // storage
        private Dictionary<int, string> dict;

        public UndoRedo( int size )
        {
            this.size = ( size > 0 ) ? size : 1;
            idx = 0;
            idr = 0;
            dict = new Dictionary<int, string>( size );
        }

        public bool CanUndo()
        {
            return ( ( idx ) > 1 );
        }

        public bool CanRedo()
        {
            return ( idx < idr );
        }

        public bool Add( string value )
        {
            if ( idx == size )
            {
                // rewrite items
                for ( int i = 1; i < size; i++ )
                {
                    string s = string.Empty;
                    dict.TryGetValue( i + 1, out s );
                    dict[i] = s;
                }
            }
            else
            {
                idx++;
            }
            // !important actual redo size
            // updating after new record
            idr = idx;

            dict[idx] = value;
            return true;
        }

        public bool Undo()
        {
            if ( idx > 1 )
            {
                idx--;
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Redo()
        {
            if ( idx < idr )
            {
                idx++;
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Reset()
        {
            dict = new Dictionary<int, string>( size );
            return true;
        }

        public string GetValue()
        {
            return ( idx > 0 ) ? dict[idx] : string.Empty;
        }
    }
}