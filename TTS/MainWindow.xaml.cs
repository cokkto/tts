﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using Internationalization;
using TTS.LibFSM;
using TTS.LibUndo;
using TTS.LibWorker;
using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit;
using Microsoft.Kinect.Toolkit.Controls;
using Coding4Fun.Kinect.Wpf;

namespace TTS
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // Thread for Worker
        private Thread oThread;

        // UndoRedo
        private static UndoRedo oText;

        // Kinect settings
        private KinectSensorChooser sensorChooser;

        #region MainWindow

        /// <summary>
        /// Initialize components
        /// </summary>
        public MainWindow()
        {
            // WPF required
            InitializeComponent();
            // Init Finite State Machine
            Status.Init();
        }

        private void Window_Loaded( object sender, RoutedEventArgs e )
        {
            SetInterfaceInternationalization();
            label1.Content = String.Empty;
            buttonUndo.IsEnabled = false;
            buttonRedo.IsEnabled = false;
            WorkerStart();
            oText = new UndoRedo( 5 );
            // Important! Init UndoRedo
            oText.Add( textBox1.Text );
            // Attach Status events
            Status.HasChanged += this.Form_StatusChanged;
            // cold Status event launch, otherwise first button click would not beeing handled
            Status.RaiseEvent();
            // Attach Kinect events
            this.sensorChooser = new KinectSensorChooser();
            this.sensorChooser.KinectChanged += SensorChooserOnKinectChanged;
            this.sensorChooserUi.KinectSensorChooser = this.sensorChooser;
            try
            {
                this.sensorChooser.Start();

            }
            catch ( Exception )
            {

                throw;
            }
        }

        private void Window_Closing( object sender, System.ComponentModel.CancelEventArgs e )
        {
            // stop Worker
            WorkerStop();
            
        }

        private void Window_Closed( object sender, System.EventArgs e )
        {
         }

        #endregion MainWindow

        #region Events

        /// <summary>
        /// Manage form elements according to current state.
        /// Note: FSM is launched in a separate thread,
        /// thus all operations should be processed asynchronously
        /// by invoking in System.Dispatcher.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_StatusChanged( object sender, System.EventArgs e )
        {
            Debug.WriteLine( "FS event: " + Status.CurrentState );
            switch ( Status.CurrentState )
            {
                case State.Inactive:
                    AsyncKinectButtonContent( buttonRecord, I18N.BTN_RECORD_ON );
                    AsyncKinectButtonEnabled( buttonClear, true );
                    AsyncKinectButtonEnabled( buttonCopy, true );
                    AsyncKinectButtonEnabled( buttonUndo, oText.CanUndo() );
                    AsyncKinectButtonEnabled( buttonRedo, oText.CanRedo() );
                    break;

                case State.Recording:
                    AsyncKinectButtonContent( buttonRecord, I18N.BTN_RECORD_OFF );
                    // Block elements while processing data
                    AsyncKinectButtonEnabled( buttonClear, false );
                    AsyncKinectButtonEnabled( buttonCopy, false );
                    AsyncKinectButtonEnabled( buttonUndo, false );
                    AsyncKinectButtonEnabled( buttonRedo, false );
                    break;

                case State.Recorded:
                    AsyncKinectButtonContent( buttonRecord, I18N.BTN_RECORD_CANCEL );
                    break;

                case State.Converting:
                    break;

                case State.Converted:
                    AsyncKinectButtonContent( buttonRecord, I18N.BTN_RECORD_ON );
                    // Buttons Undo and Redo handled together with text
                    AsyncTextContent( textBox1, Worker.dataPipe.Recognised );
                    AsyncKinectButtonEnabled( buttonClear, true );
                    AsyncKinectButtonEnabled( buttonCopy, true );
                    break;

                case State.Terminated:
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// Set actions for Record button according to current state
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonRecord_Click( object sender, RoutedEventArgs e )
        {
            Debug.WriteLine( "BTN event: " + Status.CurrentState );
            switch ( Status.CurrentState )
            {
                case State.Inactive:
                    AsyncKinectButtonContent( buttonRecord, I18N.BTN_RECORD_OFF );
                    Status.MoveNext( Signal.StartRecording );
                    break;

                case State.Recording:
                    Status.MoveNext( Signal.StopRecording );
                    break;

                case State.Recorded:
                    WorkerReset();
                    break;

                case State.Converting:
                    WorkerReset();
                    break;

                case State.Converted:
                    break;

                case State.Terminated:
                    break;

                default:
                    break;
            }
            Debug.WriteLine( "BTN postevent: " + Status.CurrentState );
        }

        private void buttonCopy_Click( object sender, RoutedEventArgs e )
        {
            Clipboard.SetText( textBox1.Text );
        }

        private void buttonUndo_Click( object sender, RoutedEventArgs e )
        {
            // TODO: No need to have double check - possibility already checkd in FormUpdate
            if ( oText.Undo() )
            {
                textBox1.Text = oText.GetValue();
            }
            // Handle Undo Redo buttons
            AsyncKinectButtonEnabled( buttonUndo, oText.CanUndo() );
            AsyncKinectButtonEnabled( buttonRedo, oText.CanRedo() );
        }

        private void buttonRedo_Click( object sender, RoutedEventArgs e )
        {
            // TODO: No need to have double check - possibility already checkd in FormUpdate
            if ( oText.Redo() )
            {
                textBox1.Text = oText.GetValue();
            }
            // Handle Undo Redo buttons
            AsyncKinectButtonEnabled( buttonUndo, oText.CanUndo() );
            AsyncKinectButtonEnabled( buttonRedo, oText.CanRedo() );
        }

        private void buttonClear_Click( object sender, RoutedEventArgs e )
        {
            AsyncTextContent( textBox1 );
        }

        #endregion Events

        #region Kinect Events

        private void SensorChooserOnKinectChanged( object sender, KinectChangedEventArgs args )
        {
            bool error = false;
            if ( args.OldSensor != null )
            {
                try
                {
                    args.OldSensor.DepthStream.Range = DepthRange.Default;
                    args.OldSensor.SkeletonStream.EnableTrackingInNearRange = false;
                    args.OldSensor.DepthStream.Disable();
                    args.OldSensor.SkeletonStream.Disable();
                }
                catch ( InvalidOperationException )
                {
                    // KinectSensor might enter an invalid state while enabling/disabling streams or stream features.
                    // E.g.: sensor might be abruptly unplugged.
                    error = true;
                }
            }

            if ( args.NewSensor != null )
            {
                try
                {
                    args.NewSensor.DepthStream.Enable( DepthImageFormat.Resolution640x480Fps30 );
                    args.NewSensor.SkeletonStream.Enable();

                    try
                    {
                        args.NewSensor.DepthStream.Range = DepthRange.Near;
                        args.NewSensor.SkeletonStream.EnableTrackingInNearRange = true;
                        args.NewSensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Seated;
                    }
                    catch ( InvalidOperationException )
                    {
                        // Non Kinect for Windows devices do not support Near mode, so reset back to default mode.
                        args.NewSensor.DepthStream.Range = DepthRange.Default;
                        args.NewSensor.SkeletonStream.EnableTrackingInNearRange = false;
                        error = true;
                    }
                }
                catch ( InvalidOperationException )
                {
                    error = true;
                    // KinectSensor might enter an invalid state while enabling/disabling streams or stream features.
                    // E.g.: sensor might be abruptly unplugged.
                }
            }

            if (!error)
                kinectRegion.KinectSensor = args.NewSensor;
        }
        #endregion Kinect Events

                #region Worker

        private void WorkerStart()
        {
            // Start worker thread
            oThread = new Thread( new ThreadStart( Worker.DoWork ) );
            oThread.Start();
            while ( !oThread.IsAlive )
                ;
        }

        private void WorkerStop()
        {
            // Stop worker thread
            try
            {
                oThread.Abort();
            }
            catch ( Exception )
            {
            }
            finally
            {
                try
                {
                    oThread.Join();
                }

                catch ( Exception )
                {
                }
            }
        }

        private void WorkerReset()
        {
            try
            {
                Status.MoveNext( Signal.Idle );
            }
            catch ( Exception )
            {
                Status.Init();
            }
        }

        #endregion Worker

        #region Async

        private void AsyncButtonContent( Button control, string text )
        {
            Application.Current.Dispatcher.BeginInvoke( new Action( () =>
            {
                try
                {
                    control.SetValue( ContentControl.ContentProperty, text );
                }
                catch ( Exception )
                {
                }
            } ) );
        }


        private void AsyncKinectButtonContent( KinectTileButton control, string text )
        {
            Application.Current.Dispatcher.BeginInvoke( new Action( () =>
            {
                try
                {
                    control.Label = text;
                }
                catch ( Exception )
                {
                }
            } ) );
        }

        private void AsyncButtonEnabled( Button control, bool p )
        {
            Application.Current.Dispatcher.BeginInvoke( new Action( () =>
            {
                try
                {
                    control.IsEnabled = p;
                }
                catch ( Exception )
                {
                }
            } ) );
        }

        private void AsyncKinectButtonEnabled( KinectTileButton control, bool p )
        {
            Application.Current.Dispatcher.BeginInvoke( new Action( () =>
            {
                try
                {
                    control.IsEnabled = p;
                }
                catch ( Exception )
                {
                }
            } ) );
        }

        private void AsyncTextContent( TextBox control, string value )
        {
            Application.Current.Dispatcher.BeginInvoke( new Action( () =>
            {
                try
                {
                    control.Text += ( value + ConfigurationManager.AppSettings["StringOptsEOL"] );
                    oText.Add( control.Text );

                    // Handle Undo Redo buttons
                    AsyncKinectButtonEnabled( buttonUndo, oText.CanUndo() );
                    AsyncKinectButtonEnabled( buttonRedo, oText.CanRedo() );
                }
                catch ( Exception )
                {
                }
            } ) );
        }

        private void AsyncTextContent( TextBox control )
        {
            Application.Current.Dispatcher.BeginInvoke( new Action( () =>
            {
                try
                {
                    control.Text = string.Empty;
                    oText.Add( control.Text );

                    // Handle Undo Redo buttons
                    AsyncKinectButtonEnabled( buttonUndo, oText.CanUndo() );
                    AsyncKinectButtonEnabled( buttonRedo, oText.CanRedo() );
                }
                catch ( Exception )
                {
                }
            } ) );
        }

        #endregion Async

        private void SetInterfaceInternationalization() 
        {
            buttonCopy.Label = ConfigurationManager.AppSettings["BtnCopy"];
            buttonUndo.Label = ConfigurationManager.AppSettings["BtnUndo"];
            buttonRedo.Label = ConfigurationManager.AppSettings["BtnRedo"];
            buttonClear.Label = ConfigurationManager.AppSettings["BtnClear"];
            this.Title = ConfigurationManager.AppSettings["WindowName"];
        }
    
    }
}