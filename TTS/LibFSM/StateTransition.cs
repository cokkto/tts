﻿namespace TTS.LibFSM
{
    public class StateTransition
    {
        private readonly State state;
        private readonly Signal signal;

        public StateTransition( State currentState, Signal command )
        {
            state = currentState;
            signal = command;
        }

        public override int GetHashCode()
        {
            return 17 + 31 * state.GetHashCode() + 31 * signal.GetHashCode();
        }

        public override bool Equals( object obj )
        {
            StateTransition other = obj as StateTransition;
            return other != null && this.state == other.state && this.signal == other.signal;
        }
    }
}