﻿namespace TTS.LibFSM
{
    public enum Signal
    {
        StartRecording,
        StopRecording,
        StartConversion,
        StopConversion,
        Terminate,
        Idle
    }
}