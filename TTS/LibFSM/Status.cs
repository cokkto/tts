﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace TTS.LibFSM
{
    public class Status
    {
        private static readonly Lazy<Status> lazy = new Lazy<Status>( () => new Status() );

        public static Status Instance
        {
            get
            {
                return lazy.Value;
            }
        }

        private Status()
        {
            Init();
        }

        public static event EventHandler HasChanged;

        public static void RaiseEvent()
        {
            EventHandler handler = HasChanged;
            if ( handler != null )
            {
                handler( Status.Instance, EventArgs.Empty );
            }
        }

        public static State GetNext( Signal signal )
        {
            StateTransition transition = new StateTransition( CurrentState, signal );
            Debug.WriteLine( ( transition != null ) ? "GetNext transition " + signal.ToString() : "GetNext transition NULL" );
            State nextState;
            if ( !Transitions.TryGetValue( transition, out nextState ) )
            {
                Debug.WriteLine( "GetNext Exception " + CurrentState + " -> " + signal );
                throw new Exception( "Invalid transition: " + CurrentState + " -> " + signal );
            }
            Debug.WriteLine( "GetNext Rise event " + nextState );
            return nextState;
        }

        public static State MoveNext( Signal signal )
        {
            CurrentState = GetNext( signal );
            RaiseEvent();
            return CurrentState;
        }

        public static void Init()
        {
            CurrentState = State.Inactive;
            Transitions = new Dictionary<StateTransition, State>
            {
                { new StateTransition(State.Inactive, Signal.StartRecording), State.Recording },
                { new StateTransition(State.Inactive, Signal.Idle), State.Inactive },
                { new StateTransition(State.Inactive, Signal.Terminate), State.Terminated },
                { new StateTransition(State.Recording, Signal.StopRecording), State.Recorded },
                { new StateTransition(State.Recording, Signal.Idle), State.Inactive },
                { new StateTransition(State.Recording, Signal.Terminate), State.Terminated },
                { new StateTransition(State.Recorded, Signal.StartConversion), State.Converting },
                { new StateTransition(State.Recorded, Signal.Idle), State.Inactive },
                { new StateTransition(State.Recorded, Signal.Terminate), State.Terminated },
                { new StateTransition(State.Converting, Signal.StopConversion), State.Converted },
                { new StateTransition(State.Converting, Signal.Idle), State.Inactive },
                { new StateTransition(State.Converting, Signal.Terminate), State.Terminated },
                { new StateTransition(State.Converted, Signal.Idle), State.Inactive },
                { new StateTransition(State.Converted, Signal.Terminate), State.Terminated },
                { new StateTransition(State.Terminated, Signal.Terminate), State.Terminated }
            };
        }

        public static State CurrentState
        {
            get;
            set;
        }

        public static Dictionary<StateTransition, State> Transitions
        {
            get;
            set;
        }
    }
}