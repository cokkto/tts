﻿namespace TTS.LibFSM
{
    public enum State
    {
        Inactive,
        Recording,
        Recorded,
        Converting,
        Converted,
        Terminated
    }
}