﻿namespace Internationalization
{
    internal class I18N
    {
        public static string BTN_RECORD_ON = "Record";
        public static string BTN_RECORD_OFF = "Stop";
        public static string BTN_RECORD_CANCEL = "Cancel";
    }
}