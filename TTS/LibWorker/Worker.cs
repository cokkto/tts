﻿using System;
using System.Diagnostics;
using System.Threading;
using TTS.LibFSM;

namespace TTS.LibWorker
{
    public class Worker
    {
        public static string TestString = "test  ";
        public static TTS.LibRecognize.VoiceTextData dataPipe;
        public static string tmpFile;
        public static TTS.LibWave.Recorder rec;

        public static void DoWork()
        {
            Debug.WriteLine( "DoWork In" );
            try
            {
                // main process body

                while ( Status.CurrentState != State.Terminated )
                {
                    switch ( Status.CurrentState )
                    {
                        case State.Inactive:
                            break;

                        case State.Recording:
                            StartRecorder();
                            break;

                        case State.Recorded:
                            StopRecorder();
                            break;

                        case State.Converting:
                            StartConversion();
                            break;

                        case State.Converted:
                            StopConversion();
                            break;

                        case State.Terminated:
                            break;

                        default:
                            // Idle
                            break;
                    }

                    //Debug.WriteLine( "DoWork In Switch" );
                }
            }

            catch ( Exception )
            {
                // before termination
                Debug.WriteLine( "DoWork Terminating" );
            }
            // here goes second unhandled ThreadAbortException! that can be prevented with Thread.ResetAbort()
            finally
            {
                // after second ThreadAbortException after process has been terminated
                Debug.WriteLine( "DoWork Finally Terminated" );
            }
        }

        private static void StartRecorder()
        {
            Debug.WriteLine( "Start record" );

            Thread oThread = new Thread( new ThreadStart( WorkerRecorder.DoWork ) );
            oThread.Start();

            while ( !oThread.IsAlive )
                ;

            while ( Status.CurrentState == State.Recording )
            {
            }
            try
            {
                oThread.Abort();
                oThread.Join();
            }
            catch ( Exception )
            {
            }
            Debug.WriteLine( "Recorded" );
        }

        private static void StopRecorder()
        {
            Debug.WriteLine( "Stop record" );
            Thread.Sleep( 100 );
            try
            {
                Status.MoveNext( Signal.StartConversion );
            }
            catch ( Exception )
            {
                Status.MoveNext( Signal.Idle );
            }
        }

        private static void StartConversion()
        {
            Debug.WriteLine( "Start conversion" );
            WorkerConverter.DoWork();
            try
            {
                Status.MoveNext( Signal.StopConversion );
            }
            catch ( Exception )
            {
                Status.MoveNext( Signal.Idle );
            }
        }

        private static void StopConversion()
        {
            Debug.WriteLine( "Stop conversion" );
            Thread.Sleep( 1000 );
            try
            {
                Status.MoveNext( Signal.Idle );
            }
            catch ( Exception )
            {
                Status.MoveNext( Signal.Idle );
            }
        }
    }
}