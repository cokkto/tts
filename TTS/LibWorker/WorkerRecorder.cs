﻿using TTS.LibWave;

namespace TTS.LibWorker
{
    public class WorkerRecorder
    {
        public static void DoWork()
        {
            // restart data
            Worker.dataPipe = null;
            // get tmp filename
            Worker.tmpFile = System.IO.Path.GetTempFileName();// +".flac";
            // begin recording
            Worker.rec = new Recorder( Worker.tmpFile );

            System.Diagnostics.Debug.WriteLine( "..................  REC ......................." );

            try
            {
                // Continue recording infinitely until Stop signal
                while ( true )
                {
                }
            }
            catch ( System.Threading.ThreadAbortException )
            {
                // use Thread.Abort(Object) if necessary
                //Console.WriteLine( (string) abortException.ExceptionState );

                // finalize file and suppress garbage collection
                Worker.rec.Stop();
                while ( !Worker.rec.AllDone )
                {
                }
                System.GC.SuppressFinalize( Worker.rec );
            }
        }
    }
}