﻿using System;
using System.IO;
using TTS.LibRecognize;

namespace TTS.LibWorker
{
    internal class WorkerConverter
    {
        public static void DoWork()
        {
            GoogleRecognizer recogn = new GoogleRecognizer();
            GoogleResponse respdata = null;
            try
            {
                respdata = recogn.RecognizeFlac( Worker.tmpFile );
            }
            catch ( Exception )
            {
                respdata = null;
            }
            if ( respdata == null )
            {
                Worker.dataPipe = null;
            }
            else
            {
                Worker.dataPipe = new VoiceTextData()
                {
                    Rating = respdata.hypotheses[0].confidence,
                    Recognised = respdata.hypotheses[0].utterance
                };
            }
            File.Delete( Worker.tmpFile );
        }
    }
}